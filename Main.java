public class Main {
        public static void main(String[] args) {
            int a = 154;
            int b = 44;

            int HOK = countingHOK(a, b);
            int HOD = countingHOD(a, b);

            System.out.println(HOK);
            System.out.println(HOD);
        }

        public static int countingHOK(int a, int b) {
            int min = Math.min(a, b);
            int max = Math.max(a, b);
            int mod = max % min;

            while (mod != 0) {
                int temp = mod;
                mod = min % mod;
                min = temp;
            }

            return min;
        }

        public static int countingHOD(int a, int b) {
            int HOK = countingHOK(a, b);
            int HOD = a * b / HOK;
            return HOD;
        }
    }

