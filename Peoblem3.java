public class Peoblem3 {
    public static int smallestCommonMultiple(int a, int b, int c) {
        int[] factors = new int[a + b + c + 1];//массив factors хранит наивисшие степени каждого простого множителя
        for (int n : new int[]{a, b, c}) {
            int[] nFactors = primeFactors(n);
            for (int i = 0; i < nFactors.length; i++) {
                factors[i] = Math.max(factors[i], nFactors[i]);
            }
        }
        int result = 1;
        for (int i = 2; i < factors.length; i++) {
            if (factors[i] > 0) {
                result *= Math.pow(i, factors[i]);
            }
        }
        return result;
    }

    public static int[] primeFactors(int n) {//метод находит простое разложение каждого числа и записывает в массив factors
        int[] factors = new int[n + 1];
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                factors[i]++;
                n /= i;
            }//перебирает массив factors находит НОК умножением каждого простого множителя на наивысшую степень
        }
        return factors;
    }
}
