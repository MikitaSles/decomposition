public class Problem2 {
    public static void main(String[] args) {
        int a = 92;
        int b = 46;
        int c = 23;
        int d = 115;
        int[] values = {a, b, c, d};

        int gcdMultiple = elementCountGCD(values);

        System.out.println(gcdMultiple);
    }

    public static int elementCountGCD (int[] values) {
        int currentGcd = countGCD(values[0], values[1]);

        for (int i = 2; i < values.length; i++) {
            currentGcd = countGCD(currentGcd, values[i]);
        }

        return currentGcd;
    }

    public static int countGCD(int a, int b) {
        int min = Math.min(a, b);
        int max = Math.max(a, b);
        int mod = max % min;

        while (mod != 0) {
            int temp = mod;
            mod = min % mod;
            min = temp;
        }

        return min;
    }
}
