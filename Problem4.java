public class Problem4 {
    public static int sumOddFactorials() {//вычисляем факториалы всех чисел от 0 до 9
        int[] factorials = new int[10];
        factorials[0] = 1;
        for (int i = 1; i < factorials.length; i++) {
            factorials[i] = factorials[i-1] * i;
        }
        int sum = 0;
        for (int i = 1; i <= 9; i += 1) {//суммируем факториалы в переменную "sum"
            sum += factorials[i];
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(sumOddFactorials());
    }
}
