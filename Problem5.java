import java.util.Arrays;

public class Problem5 {
    public static int findSecondLargest(int[] arr) {
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                secondMax = max;
                max = arr[i];
            } else if (arr[i] > secondMax && arr[i] != max) {
                secondMax = arr[i];
            }
        }
        return secondMax;
    }

    public static void main(String[] args) {
        int[] arr = {54, 53, 52, 35, 736, 212, 8, 4, 6};
        int secondLargest = findSecondLargest(arr);
        System.out.println("The second largest number is: " + secondLargest);
    }
}